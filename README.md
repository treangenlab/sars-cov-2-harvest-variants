# SARS-CoV-2 Harvest Variants

SARS-CoV-2 Harvest Variants is a pipeline that handles data downloading, quality control, read mapping, intra-host variant calling, genome phasing and MSA for SARS-CoV-2 SRAs and nucleotide records.

## Installing SARS-CoV-2 Harvest Variants

SARS-CoV-2 Harvest Variants is pipeline in python scripts which requires python>=3.7. The easiest way to install all the dependencies is through Anaconda or Miniconda. If you do not have conda installed on your system, please follow the instructions [here](https://conda.io/projects/conda/en/latest/user-guide/install/index.html). First, download the latest version of Miniconda with:
```
wget https://repo.anaconda.com/miniconda/Miniconda3-4.7.12.1-Linux-x86_64.sh
```
Then run the following command and follow the prompts on the screens:
```
bash Miniconda3-4.7.12.1-Linux-x86_64.sh
```
Restart your terminal and try the following command to ensure that Miniconda is installed correctly:
```
conda --version
```
Once you have Miniconda installed, first run the following commands to performed a one-time set up of Bioconda, you can read more about Bioconda configuration [here](https://bioconda.github.io/).
```
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge
```
Then create a conda environment with Python 3.7.5, you can read more about managing conda environment [here](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html).
```
conda create --name harvest_variants python=3.7.5
```
Activate the environment we just created with:
```
conda activate harvest_variants
```
To install SARS-CoV-2 Harvest Variants from source, run the following commands:
```
git clone https://gitlab.com/treangenlab/sars-cov-2-harvest-variants.git
cd sars-cov-2-harvest-variants
git checkout v1.1.0
./install.sh
```
To install SARS-CoV-2 Harvest Variants from conda, run the following command:
```
conda install -c bioconda harvest-variants=1.1.0
```

### System requirements
Variabel was tested on Ubuntu 18. The packages used for testing are listed below:
* Python 3.7.5
* pyvcf 0.6.8
* Biopython 1.79
* sra-tools 2.10.9
* Parsnp 1.7.4
* fastp 0.23.2
* samtools 1.15
* bcftools 1.15
* bedtools 2.30.0
* BWA 0.7.17-r1188
* lofreq 2.1.5
* snpEff 5.1
* fastqc 0.11.9

## Quick start and demo run

The pipeline takes either a text file containing a list of sra run IDs or a text file of a list of NCBI Nucleotide Accession IDs or both as input. For examples of the input file, please refer to `example_acc_id_list.txt` and `example_sra_id_list.txt`. In order to download nucleotide records from NCBI, the user must provide an email address to identifies himself/herself. A reference genome in fasta format is also required. To run the pipeline:
```
hv-pipeline.py -r data/reference_file.fasta -s examples/example_sra_id_list.txt -a examples/example_acc_id_list.txt -email <user_email>
```

## Running SARS-CoV-2 Harvest Variants

Variabel requires 2 types of input:
1. A fasta reference genome
2. A text file containing list of sra run IDs or NCBI Nucleotide Accession IDs

all output will be produced in `output/` unless an alternative is specified with the `-o` flag. The final output VCF file and MSA file is located at `output/parsnp_output/`
Please see the help documentation for other parameters.

```
usage: hv-pipeline.py [-h] -r REFERENCE [-o OUTPUT] [-numcore NUMCORE] [-a ACCESSION_ID_LIST] [-email EMAIL]
                      [-raw-seq-dir RAW_SEQ_DIR] [-s SRA_ID_LIST] [-overwrite] [-raw-read-dir RAW_READ_DIR]
                      [-slidingWindowSize SLIDINGWINDOWSIZE] [-slidingWindowQuality SLIDINGWINDOWQUALITY]
                      [-minLen MINLEN] [-fastpErrorCorrection] [-adapters ADAPTERS] [-min-breadth MIN_BREADTH]
                      [-min-average-depth MIN_AVERAGE_DEPTH] [--no-indel] [-min-af MIN_AF] [-min-cov MIN_COV]
                      [-end-cutoff END_CUTOFF] [-max-contiguous-n MAX_CONTIGUOUS_N] [-max-n MAX_N]
                      [-max-len-diff MAX_LEN_DIFF]

Harvest Variants

optional arguments:
  -h, --help            show this help message and exit
  -r REFERENCE, --reference REFERENCE
                        Reference genome in fasta format.
  -o OUTPUT, --output OUTPUT
                        Output directory
  -numcore NUMCORE      Number of processor used for parallelization
  --citations           Print citations of the tools used in the pipeline.
  -a ACCESSION_ID_LIST, --accession-id-list ACCESSION_ID_LIST
                        text file containing sra ids seperated by newline.
  -email EMAIL          Email to identify yourself to NCBI, required for downloading assemblies from NCBI.
  -raw-seq-dir RAW_SEQ_DIR
                        Directory to store raw genomes from NCBI nucleotide.
  -s SRA_ID_LIST, --sra-id-list SRA_ID_LIST
                        text file containing sra ids seperated by newline.
  -overwrite            Overwrite existing raw sequence fastq files.
  -raw-read-dir RAW_READ_DIR
                        Directory to store raw sequencing reads from SRA.
  -slidingWindowSize SLIDINGWINDOWSIZE
                        Sliding window size for fastp. Default: [4]
  -slidingWindowQuality SLIDINGWINDOWQUALITY
                        Sliding window quality threshold for fastp. Default: [15]
  -minLen MINLEN        Minimum length required for a read to be valid. Default: [15]
  -fastpErrorCorrection
                        Enable base correction in overlapped regions using Fastp. Default: [False]
  -adapters ADAPTERS    Fasta file containing adapter sequences.
  -min-breadth MIN_BREADTH
                        minimum breadth of coverage to pass quality control, default: [0.9]
  -min-average-depth MIN_AVERAGE_DEPTH
                        minimum average depth of coverage to pass quality control, default: [500]
  --no-indel            Do not call indels
  -min-af MIN_AF        Minimum allele frequency cutoff, default: [0.02]
  -min-cov MIN_COV      Minimum coverage for a variant to be called, default: [20]
  -end-cutoff END_CUTOFF
                        Number of bases ignored at the start and end of the genomes during qc, default: [300]
  -max-contiguous-n MAX_CONTIGUOUS_N
                        Maximum length of contiguous ambiguous bases/Ns, default: [200]
  -max-n MAX_N          Maximum number of ambiguous bases/Ns, default: [300]
  -max-len-diff MAX_LEN_DIFF
                        Maximum length difference compare to the reference genome, default: [300]
```

The user can also run the pipeline for each SRA run ids independently using `sra2vcf.py` if the user decided to accelerate the process using HPC systems.
```
./sra2vcf.py -r data/reference_file.fasta -s <single_sra_run_id>
```
The full usage of `sra2vcf.py` is listed below
```
usage: sra2vcf.py [-h] -r REFERENCE [-o OUTPUT] [-numcore NUMCORE] -s
                  SRA_RUN_ID [-overwrite] [-raw-read-dir RAW_READ_DIR]
                  [-slidingWindowSize SLIDINGWINDOWSIZE]
                  [-slidingWindowQuality SLIDINGWINDOWQUALITY]
                  [-minLen MINLEN] [-fastpErrorCorrection]
                  [-adapters ADAPTERS] [-min-breadth MIN_BREADTH]
                  [-min-average-depth MIN_AVERAGE_DEPTH] [--no-indel]
                  [-min-af MIN_AF] [-min-cov MIN_COV]

Harvest Variants

optional arguments:
  -h, --help            show this help message and exit
  -r REFERENCE, --reference REFERENCE
                        Reference genome in fasta format.
  -o OUTPUT, --output OUTPUT
                        Output directory
  -numcore NUMCORE      Number of processor used for parallelization
  --citations           Print citations of the tools used in the pipeline.
  -s SRA_RUN_ID, --sra-run-id SRA_RUN_ID
                        SRA run id
  -overwrite            Overwrite existing raw sequence fastq files.
  -raw-read-dir RAW_READ_DIR
                        Directory to store raw sequencing reads from SRA.
  -slidingWindowSize SLIDINGWINDOWSIZE
                        Sliding window size for fastp. Default: [4]
  -slidingWindowQuality SLIDINGWINDOWQUALITY
                        Sliding window quality threshold for fastp. Default: [15]
  -minLen MINLEN        Minimum length required for a read to be valid. Default: [15]
  -fastpErrorCorrection
                        Enable base correction in overlapped regions using Fastp. Default: [False]
  -adapters ADAPTERS    Fasta file containing adapter sequences.
  -min-breadth MIN_BREADTH
                        minimum breadth of coverage to pass quality control, default: [0.9]
  -min-average-depth MIN_AVERAGE_DEPTH
                        minimum average depth of coverage to pass quality control, default: [500]
  --no-indel            Do not call indels
  -min-af MIN_AF        Minimum allele frequency cutoff, default: [0.02]
  -min-cov MIN_COV      Minimum coverage for a variant to be called, default:[20]
```

## Platfoms
This software was tested on Ubuntu 18.04. It is explicilty unsupported on macOS and the Windows Subsystem for Linux.

## Support

This work is supported by the Centers for Disease Control and Prevention (CDC) contract 75D30121C11180.

## Citations

Harvest-variants depends on several open-source packages, we would like to thank all of the developers. If you use harvest-variants for your work,please cite the following tools that are foundational to our pipeline:
```
Biopython: Cock, Peter JA, et al. "Biopython: freely available Python tools for computational molecular biology and bioinformatics." Bioinformatics 25.11 (2009): 1422-1423.

PyVCF: Please cite the following url: https://github.com/jamescasbon/PyVCF and James Casbon.

SRA Toolkit: Please cite the following url: https://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?view=software and the SRA Toolkit Development Team.

Fastp: Chen, Shifu, et al. "fastp: an ultra-fast all-in-one FASTQ preprocessor." Bioinformatics 34.17 (2018): i884-i890.

FastQC: Andrews, S. (2010). FastQC: A Quality Control Tool for High Throughput Sequence Data [Online]. Available online at: http://www.bioinformatics.babraham.ac.uk/projects/fastqc/

BWA: Li, Heng, and Richard Durbin. "Fast and accurate short read alignment with Burrows-Wheeler transform." bioinformatics 25.14 (2009): 1754-1760.

lofreq: Wilm, Andreas, et al. "LoFreq: a sequence-quality aware, ultra-sensitive variant caller for uncovering cell-population heterogeneity from high-throughput sequencing datasets." Nucleic acids research 40.22 (2012): 11189-11201.

samtools: Li, Heng, et al. "The sequence alignment/map format and SAMtools." Bioinformatics 25.16 (2009): 2078-2079.

bedtools: Quinlan, Aaron R., and Ira M. Hall. "BEDTools: a flexible suite of utilities for comparing genomic features." Bioinformatics 26.6 (2010): 841-842.

bcftools: Danecek, Petr, et al. "Twelve years of SAMtools and BCFtools." Gigascience 10.2 (2021): giab008.

SnpEff: Cingolani, Pablo, et al. "A program for annotating and predicting the effects of single nucleotide polymorphisms, SnpEff: SNPs in the genome of Drosophila melanogaster strain w1118; iso-2; iso-3." Fly 6.2 (2012): 80-92.

Parsnp: Treangen, Todd J., et al. "The Harvest suite for rapid core-genome alignment and visualization of thousands of intraspecific microbial genomes." Genome biology 15.11 (2014): 1-15.
```

## License

Copyright (c) 2022 Treangen Lab.

This software is licensed under the terms of the GNU GENERAL PUBLIC LICENSE Version 3. For details, please see the LICENSE file.

