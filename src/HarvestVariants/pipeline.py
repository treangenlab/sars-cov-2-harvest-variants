#!/usr/bin/env python

import os
import argparse
import subprocess
from Bio import Entrez

from HarvestVariants.sequence_download import download_accession
from HarvestVariants.sra_download import data_retriving
from HarvestVariants.sequence_download import download_accession
from HarvestVariants.pre_alignment_qc import run_fastp
from HarvestVariants.alignment import read_mapping_bwa, covert_samfile, index_reference
from HarvestVariants.post_alignment_qc import coverage_filter, fastqc_stats
from HarvestVariants.variant_calling import sort_bam_file, variant_call_lofreq, faidx_reference, vcf_quality_control
from HarvestVariants.genome_phasing import mask_reference, vcf_split
from HarvestVariants.genome_qc import pass_filter
from HarvestVariants.msa import merge_genome_dirs, run_parsnp, run_harvest_tools, delete_intermediate_files
from HarvestVariants.annotation import annotation

def parse_sra_acc_list(filename):
    id_list = []
    with open(filename, "r") as input_f:
        for line in input_f.readlines():
            sra_acc_id = line.strip()
            if sra_acc_id != "":
                id_list.append(sra_acc_id)
                print(sra_acc_id)
    return id_list

def print_citations():
    print('''harvest-variants depends on several open-source packages, we would like to thank all of the developers. If you use harvest-variants for your work,please cite the following tools that are foundational to our pipeline:

    Biopython: Cock, Peter JA, et al. \"Biopython: freely available Python tools for computational molecular biology and bioinformatics.\" Bioinformatics 25.11 (2009): 1422-1423.
    PyVCF: Please cite the following url: https://github.com/jamescasbon/PyVCF and James Casbon.
    SRA Toolkit: Please cite the following url: https://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?view=software and the SRA Toolkit Development Team.
    Fastp: Chen, Shifu, et al. \"fastp: an ultra-fast all-in-one FASTQ preprocessor.\" Bioinformatics 34.17 (2018): i884-i890.
    FastQC: Andrews, S. (2010). FastQC: A Quality Control Tool for High Throughput Sequence Data [Online]. Available online at: http://www.bioinformatics.babraham.ac.uk/projects/fastqc/
    BWA: Li, Heng, and Richard Durbin. \"Fast and accurate short read alignment with Burrows-Wheeler transform.\" bioinformatics 25.14 (2009): 1754-1760.
    lofreq: Wilm, Andreas, et al. \"LoFreq: a sequence-quality aware, ultra-sensitive variant caller for uncovering cell-population heterogeneity from high-throughput sequencing datasets.\" Nucleic acids research 40.22 (2012): 11189-11201.
    samtools: Li, Heng, et al. \"The sequence alignment/map format and SAMtools.\" Bioinformatics 25.16 (2009): 2078-2079.
    bedtools: Quinlan, Aaron R., and Ira M. Hall. \"BEDTools: a flexible suite of utilities for comparing genomic features.\" Bioinformatics 26.6 (2010): 841-842.
    bcftools: Danecek, Petr, et al. \"Twelve years of SAMtools and BCFtools.\" Gigascience 10.2 (2021): giab008.
    SnpEff: Cingolani, Pablo, et al. \"A program for annotating and predicting the effects of single nucleotide polymorphisms, SnpEff: SNPs in the genome of Drosophila melanogaster strain w1118; iso-2; iso-3.\" Fly 6.2 (2012): 80-92.
    Parsnp: Treangen, Todd J., et al. \"The Harvest suite for rapid core-genome alignment and visualization of thousands of intraspecific microbial genomes.\" Genome biology 15.11 (2014): 1-15.
        
    ''')

def main():
    '''main function'''
    parser = argparse.ArgumentParser(description="Harvest Variants")

    parser.add_argument("-r", "--reference", type=str, required=True,
                        help="Reference genome in fasta format.")
    parser.add_argument("-o", "--output", type=str, default="output",
                        help="Output directory")
    parser.add_argument("-numcore", type=int, default=1,
                        help="Number of processor used for parallelization")
    parser.add_argument("--citations", dest='citations', action='store_true',
                        help="Print citations of the tools used in the pipeline.")

    # assembly download parameters
    parser.add_argument("-a", "--accession-id-list", type=str, required=False,
                        help="text file containing sra ids seperated by newline.")
    parser.add_argument("-email", type=str, required=False,
                        help="Email to identify yourself to NCBI, required for downloading assemblies from NCBI.")
    parser.add_argument("-raw-seq-dir", type=str, default="raw_sequences",
                        help="Directory to store raw genomes from NCBI nucleotide.")

    # sra download parameters
    parser.add_argument("-s", "--sra-id-list", type=str, required=False,
                        help="text file containing sra ids seperated by newline.")
    parser.add_argument('-overwrite', dest='overwrite', action='store_true',
                        help="Overwrite existing raw sequence fastq files.")
    parser.add_argument("-raw-read-dir", type=str, default="raw_reads",
                        help="Directory to store raw sequencing reads from SRA.")

    # fastp parameters
    parser.add_argument("-slidingWindowSize", type=int, default=4,
                        help="Sliding window size for fastp. Default: [4]")
    parser.add_argument("-slidingWindowQuality", type=int, default=25,
                        help="Sliding window quality threshold for fastp. Default: [25]")
    parser.add_argument("-minLen", type=int, default=15,
                        help="Minimum length required for a read to be valid.  Default: [15]")
    parser.add_argument("-fastpErrorCorrection", dest='correction', action='store_true',
                        help="Enable base correction in overlapped regions using Fastp. Default: [False]")
    parser.add_argument("-adapters", type=str, default="",
                        help="Fasta file containing adapter sequences.")

    # post alignment QC parameters
    parser.add_argument("-min-breadth", type=float, default=0.9,
                        help="minimum breadth of coverage to pass quality control, default: [0.9]")
    parser.add_argument("-min-average-depth", type=int, default=500,
                         help="minimum average depth of coverage to pass quality control, default: [500]")

    # variant calling parameters
    parser.add_argument('--no-indel', dest='indel', action='store_false',
                        help="Do not call indels")
    parser.add_argument("-min-af", type=float, default=0.02,
                        help="Minimum allele frequency cutoff, default: [0.02]")
    parser.add_argument("-min-cov", type=int, default=20,
                        help="Minimum coverage for a variant to be called, default: [20]")
    
    # genome qc parameters
    parser.add_argument('-end-cutoff', type=int, default=300,
                        help="Number of bases ignored at the start and end of the genomes during qc, default: [300]")
    parser.add_argument("-max-contiguous-n", type=int, default=200,
                        help="Maximum length of contiguous ambiguous bases/Ns, default: [200]")
    parser.add_argument("-max-n", type=int, default=300,
                        help="Maximum number of ambiguous bases/Ns, default: [300]")
    parser.add_argument("-max-len-diff", type=int, default=300,
                        help="Maximum length difference compare to the reference genome, default: [300]")

    parser.set_defaults(overwrite=False)
    parser.set_defaults(citations=False)
    parser.set_defaults(indel=True)
    args = parser.parse_args()

    reference_file = args.reference
    output_dir = args.output

    if args.sra_id_list is not None:
        sra_list = parse_sra_acc_list(args.sra_id_list)

    if args.accession_id_list is not None:
        if args.email is not None:
            Entrez.email = args.email
            acc_list = parse_sra_acc_list(args.accession_id_list)

            raw_sequence_dir = args.raw_seq_dir
            if not os.path.exists(raw_sequence_dir):
                os.mkdir(raw_sequence_dir)
            for accession_id in acc_list:
                download_accession(accession_id, raw_sequence_dir)
        else:
            print("Email is required to download assemblies from NCBI.")
            return 0

    # global parameters
    num_cores = args.numcore

    # sra download parameters
    raw_read_dir = args.raw_read_dir
    overwrite = args.overwrite

    # pre-alignment qc parameters
    adapter_file = args.adapters
    s_window_size = args.slidingWindowSize
    s_window_q_threshold = args.slidingWindowQuality
    min_len = args.minLen
    correction = args.correction

    # post-alignment qc parameters
    min_breadth = args.min_breadth
    min_average_depth = args.min_average_depth

    # variant calling parameters
    indel_flag = args.indel
    cov_min = args.min_cov
    af_min = args.min_af

    citations = args.citations

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    if args.accession_id_list is not None:
        valid_genome_dir = "valid_genomes"
        if not os.path.exists(os.path.join(output_dir, valid_genome_dir)):
            os.mkdir(os.path.join(output_dir, valid_genome_dir))

        for accession_id in acc_list:
            if pass_filter(os.path.join(raw_sequence_dir, f"{accession_id}.fasta"), reference_file,
                                        args.end_cutoff,
                                        args.max_contiguous_n,
                                        args.max_n,
                                        args.max_len_diff):
                print(f"{accession_id} passed the quality control.")
                subprocess.run(["cp",
                                os.path.join(raw_sequence_dir, f"{accession_id}.fasta"),
                                os.path.join(output_dir, valid_genome_dir, f"{accession_id}.fasta")],
                                check=True)

        print("Merging qualified genomes into parsnp input directory...")
        merge_genome_dirs(valid_genome_dir, "parsnp_input", output_dir, "downloaded")

    if args.sra_id_list is not None:
        for sra_run_id in sra_list:
            data_retriving(sra_run_id, raw_read_dir, overwrite)

            read_1 = os.path.join(raw_read_dir, sra_run_id, f"{sra_run_id}_1.fastq")
            read_2 = os.path.join(raw_read_dir, sra_run_id, f"{sra_run_id}_2.fastq")

            # SE read created by fastq dump --split-3
            if not os.path.exists(read_1):
                read_1 = os.path.join(raw_read_dir, sra_run_id, f"{sra_run_id}.fastq")

            run_fastp(read_1,
                      read_2,
                      sra_run_id,
                      output_dir,
                      "fastp_logs",
                      adapter_file,
                      num_cores,
                      s_window_size,
                      s_window_q_threshold,
                      min_len,
                      correction)

            # BWA indexing and read mapping 
            index_reference(reference_file, output_dir)

            mapping_rt_code = read_mapping_bwa(sra_run_id, reference_file, num_cores, output_dir)
            if mapping_rt_code == 0:
                covert_samfile(sra_run_id, output_dir, num_cores)

                # BAM sorting and inserting indel qualities
                faidx_reference(reference_file, output_dir)

                sort_bam_file(sra_run_id, reference_file, output_dir, "bam_files", num_cores, indel_flag)

                # post alignment qc
                passed = coverage_filter(output_dir,
                                        "bam_files",
                                        "depth_files",
                                        sra_run_id,
                                        reference_file,
                                        min_breadth,
                                        min_average_depth)
                
                fastqc_stats(output_dir, "bam_files", sra_run_id)

                if passed:
                    print("Variant calling...")
                    variant_call_lofreq(sra_run_id, reference_file, output_dir, "bam_files", "vcf_files", num_cores, indel_flag)
                    vcf_quality_control(sra_run_id, "vcf_files", output_dir, "vcf_files_filtered", cov_min, af_min)
                    annotation(sra_run_id, "vcf_files_filtered", output_dir, "vcf_files_filtered_ann")
                    vcf_quality_control(sra_run_id, "vcf_files", output_dir, "vcf_files_consensus", cov_min, af_min=0.5)

                    mask = mask_reference(output_dir, "depth_files", "vcf_files_filtered", sra_run_id, cov_min)
                    vcf_split(sra_run_id, reference_file, output_dir, "vcf_files_filtered", mask, "variant_genomes")
                    vcf_split(sra_run_id, reference_file, output_dir, "vcf_files_consensus", mask, "consensus_genomes")
                    os.remove(mask)

                    print("Merging genomes generated from SRA into parsnp input directory...")
                    merge_genome_dirs("variant_genomes", "parsnp_input", output_dir, "variant")
                    merge_genome_dirs("consensus_genomes", "parsnp_input", output_dir, "consensus")

            else:
                print(f"{sra_run_id} read alignment fails.")
                
            print("Removing intermediate files...")
            delete_intermediate_files(sra_run_id, raw_read_dir, output_dir)

    print("Running Parsnp...")
    run_parsnp(reference_file, "parsnp_input", output_dir, num_cores)
    print("Running Harvest tools...")
    run_harvest_tools(reference_file, "parsnp_output", output_dir)
    
    if citations:
        print_citations()

if __name__ == "__main__":
    main()
