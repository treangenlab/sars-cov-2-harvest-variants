import subprocess
import os
import argparse
import shutil

def merge_genome_dirs(source_dir, dest_dir, output_dir, tag):
    if not os.path.exists(os.path.join(output_dir, dest_dir)):
        os.mkdir(os.path.join(output_dir, dest_dir))

    fasta_files = os.listdir(os.path.join(output_dir, source_dir))
    for fasta_file in fasta_files:
        new_file_name = fasta_file[0:-5] + tag + ".fasta"
        subprocess.run(["cp",
                        os.path.join(output_dir, source_dir, fasta_file),
                        os.path.join(output_dir, dest_dir, new_file_name)])

def run_parsnp(reference_genome, parsnp_input_dir, output_dir, num_cores, fast_tree_flag=False):
    genomes = os.listdir(os.path.join(output_dir, parsnp_input_dir))
    if len(genomes) >= 10 and not fast_tree_flag:
        subprocess.run(["parsnp",
                        "-r", reference_genome,
                        "-d", os.path.join(output_dir, parsnp_input_dir),
                        "-o", os.path.join(output_dir, "parsnp_output"),
                        "-p", str(num_cores),
                        "--extend-lcbs",
                        "-v"],
                        stdout=open(os.path.join(output_dir, "parsnp.log"), "a"),
                        stderr=open(os.path.join(output_dir, "parsnp.err"), "a"),
                    check=True)
    else:
        subprocess.run(["parsnp",
                        "-r", reference_genome,
                        "-d", os.path.join(output_dir, parsnp_input_dir),
                        "-o", os.path.join(output_dir, "parsnp_output"),
                        "-p", str(num_cores),
                        "--extend-lcbs",
                        "--use-fasttree",
                        "-v"],
                        stdout=open(os.path.join(output_dir, "parsnp.log"), "a"),
                        stderr=open(os.path.join(output_dir, "parsnp.err"), "a"),
                    check=True)

def run_harvest_tools(reference_genome, parsnp_output_dir, output_dir):
    subprocess.run(["harvesttools",
                "-i", os.path.join(output_dir, parsnp_output_dir, "parsnp.ggr"),
                "-x", os.path.join(output_dir, parsnp_output_dir, "parsnp.xmfa"),
                "-V", os.path.join(output_dir, parsnp_output_dir, "harvest_tools.vcf")],
                stdout=open(os.path.join(output_dir, "harvest.log"), "a"),
                stderr=open(os.path.join(output_dir, "harvest.err"), "a"),
            check=True)

def delete_intermediate_files(sra_run_id, raw_read_dir, output_dir):
    def remove_file(intermediate_file):
        if os.path.exists(intermediate_file):
            os.remove(intermediate_file)

    raw_reads = os.path.join(raw_read_dir, sra_run_id)
    if os.path.isdir(raw_reads):
        shutil.rmtree(raw_reads)

    # KEEP unfiltered vcfs at the moment.
    # unfiltered_vcf = os.path.join(output_dir, "vcf_files", f"{sra_run_id}.vcf")
    # remove_file(unfiltered_vcf)

    bam_file_1 = os.path.join(output_dir, "bam_files", f"{sra_run_id}.bam")
    bam_file_2 = os.path.join(output_dir, "bam_files", f"{sra_run_id}.sorted.bam")
    bam_file_3 = os.path.join(output_dir, "bam_files", f"{sra_run_id}.sorted.bam.bai")
    remove_file(bam_file_1)
    remove_file(bam_file_2)
    remove_file(bam_file_3)

    sam_file = os.path.join(output_dir, "sam_files", f"{sra_run_id}.sam")
    remove_file(sam_file)

    trimmed_read_1 = os.path.join(output_dir, "trimmed_reads", f"{sra_run_id}_1.fastq")
    trimmed_read_2 = os.path.join(output_dir, "trimmed_reads", f"{sra_run_id}_2.fastq")
    remove_file(trimmed_read_1)
    remove_file(trimmed_read_2)

    depth_file = os.path.join(output_dir, "depth_files", f"{sra_run_id}.depth")
    remove_file(depth_file)