'''filter assemblies based on quality of the sequence'''
import os
import argparse

from Bio import SeqIO
from Bio import Align

def count_n(seq):
    '''count the number of Ns in a sequence, as well as the maximum number of contiguous Ns'''
    total_n = 0
    contiguous_n = 0
    max_contiguous_n = 0
    for base in seq:
        if base in ('N', 'n'):
            total_n += 1
            contiguous_n += 1
            if contiguous_n > max_contiguous_n:
                max_contiguous_n = contiguous_n
        else:
            contiguous_n = 0
    return max_contiguous_n, total_n

def is_length_valid(reference, record, threshold):
    ''' return if the length of the genome is valid '''
    return  len(reference.seq)-threshold <= len(record.seq) <= len(reference.seq)+threshold

def is_pairwise_aligned(reference, record, end_cutoff):
    '''remove any sequence with insertions, deletions, and ambiguous base in core region'''
    # initialize pair-wise aligner parameters
    aligner = Align.PairwiseAligner()
    aligner.match_score = 2
    aligner.mismatch_score = -1
    aligner.open_gap_score = -0.5
    aligner.extend_gap_score = -0.1
    aligner.target_end_gap_score = 0.0
    aligner.query_end_gap_score = 0.0
    aligner.mode = 'global'

    def check_forward_reverse(aligner, reference, query_seq, end_cutoff):
        alignments = aligner.align(reference.seq, query_seq)
        alignment = format(alignments[0]).strip().split("\n") # first alignment
        aligned_reference = alignment[0]
        aligned_query = alignment[-1]

        is_filter_passed = True
        # - in reference genome indicates insertion
        if '-' in aligned_reference[end_cutoff:len(aligned_reference)-end_cutoff]:
            is_filter_passed = False
        # - in query genome indicates deletion
        if '-' in aligned_query[end_cutoff:len(aligned_query)-end_cutoff] or\
               'n' in aligned_query[end_cutoff:len(aligned_query)-end_cutoff] or\
               'N' in aligned_query[end_cutoff:len(aligned_query)-end_cutoff]:
            is_filter_passed = False
        return is_filter_passed

    try:
        return check_forward_reverse(aligner, reference, record.seq.upper(), end_cutoff) \
               or check_forward_reverse(aligner, reference, record.seq.reverse_complement().upper(), end_cutoff)
    except ValueError as e:
        bad_letters = set(list(record.seq)) - set(["A", "T", "G", "C"])
        logger.error(f"\n\tSequence {record.id} contains letters not in the alphabet. Filtering it out")
        logger.error(f"\n\tBad letters: {', '.join(bad_letters)}")
        return False

def pass_filter(genome, reference_file, end_cutoff, contiguous_n_threshold, total_n_threshold, length_threshold):
    record = SeqIO.read(genome, "fasta")
    reference = SeqIO.read(reference_file, "fasta")
    max_contiguous_n, total_n = count_n(record.seq)
    return max_contiguous_n < contiguous_n_threshold and total_n <= total_n_threshold \
            and "-" not in str(record.seq) \
            and is_length_valid(reference, record, length_threshold)
            # and is_pairwise_aligned(reference, record, end_cutoff)