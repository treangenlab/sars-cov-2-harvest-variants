"""This module is responsible for data retriveing"""
import os
import subprocess
import argparse
import shutil

def data_retriving(sra_run_id, read_dir, overwrite):
    '''download datasets from NCBI'''
    if not os.path.exists(read_dir):
        os.mkdir(read_dir)

    if overwrite and os.path.exists(os.path.join(read_dir, sra_run_id)):
        shutil.rmtree(os.path.join(read_dir, sra_run_id))

    if not os.path.exists(os.path.join(read_dir, sra_run_id)):
        # subprocess.run(["prefetch", sra_run_id],
        #                 check=True)
        os.mkdir(os.path.join(read_dir, sra_run_id))
        subprocess.run(["fasterq-dump",
                        "--outdir", os.path.join(read_dir, sra_run_id),
                        sra_run_id],
                    check=True)

def main():
    '''main function'''
    parser = argparse.ArgumentParser(description="Download data from NCBI SRA database")
    parser.add_argument("-sra-run-id", type=str, required=True, help="SRA Run ID")
    parser.add_argument("-raw-read-dir", type=str, required=True, help="Directories that stores the raw read files")
    parser.add_argument('-overwrite', dest='overwrite', action='store_true',
                        help="Overwrite existing files.")
    parser.set_defaults(overwrite=False)
    args = parser.parse_args()

    sra_run_id = args.sra_run_id
    raw_read_dir = args.raw_read_dir
    overwrite = args.overwrite

    data_retriving(sra_run_id, raw_read_dir, overwrite)

    read_1 = os.path.join(raw_read_dir, sra_run_id, f"{sra_run_id}_1.fastq")
    read_2 = os.path.join(raw_read_dir, sra_run_id, f"{sra_run_id}_2.fastq")

    # SE read created by fastq dump --split-3
    if os.path.exists(read_1) and os.path.exists(read_2):
        print(read_1)
        print(read_2)
    else:
        read_1 = os.path.join(raw_read_dir, sra_run_id, f"{sra_run_id}.fastq")
        if os.path.exists(read_1):
            print(read_1)
        else:
            print("Error.")

if __name__ == "__main__":
    main()