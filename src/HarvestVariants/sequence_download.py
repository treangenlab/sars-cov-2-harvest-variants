import time
import os
import argparse

from Bio import Entrez

def download_accession(accession_id, raw_sequence_dir):
    handle = Entrez.efetch(db="nucleotide", id=accession_id, rettype="fasta", retmode="text")
    output_f = os.path.join(raw_sequence_dir, f"{accession_id}.fasta")
    with open(output_f, "w") as output_fasta:
        output_fasta.write(handle.read())
    time.sleep(0.5)

def main():
    '''main function'''
    parser = argparse.ArgumentParser(description="Download data from NCBI Nucleotide database")
    parser.add_argument("-email", type=str, required=True, help="Email to identify yourself to NCBI")
    parser.add_argument("-accession-id", type=str, required=True, help="Accession ID")
    parser.add_argument("-raw-sequence-dir", type=str, required=True, help="Directories that stores the fasta files")
    parser.add_argument('-overwrite', dest='overwrite', action='store_true',
                        help="Overwrite existing files.")
    parser.set_defaults(overwrite=False)
    args = parser.parse_args()

    accession_id = args.accession_id
    raw_sequence_dir = args.raw_sequence_dir
    overwrite = args.overwrite

    Entrez.email = args.email
    if not os.path.exists(raw_sequence_dir):
        os.mkdir(raw_sequence_dir)

    download_accession(accession_id, raw_sequence_dir)

if __name__ == "__main__":
    main()