import os
import subprocess
import argparse

from Bio import SeqIO

def run_depth(output_dir, bam_file_dir, depth_file_dir, sample_id):
    subprocess.run(["samtools", "depth",
                    "-aa",
                    os.path.join(output_dir, bam_file_dir, f"{sample_id}.sorted.bam"),
                    "-o", os.path.join(output_dir, depth_file_dir, f"{sample_id}.depth")],
                    stdout=open(os.path.join(output_dir, "samtools.log"), "a"),
                    stderr=open(os.path.join(output_dir, "samtools.err"), "a"),
                    check=True)

    return os.path.join(output_dir, depth_file_dir, f"{sample_id}.depth")

def coverage_calc(depth_f, ref, min_depth_to_calc_breadth=20):
    record = SeqIO.read(ref, "fasta")
    ref_genome_length = len(record.seq)
    with open(depth_f) as depth:
        valid_pos = 0
        total_cov = 0
        for line in depth.readlines():
            _, pos, cov = line.strip().split("\t")
            total_cov += int(cov)
            if int(cov) >= min_depth_to_calc_breadth:
                valid_pos += 1
                
    return valid_pos/ref_genome_length, total_cov/ref_genome_length

def coverage_filter(output_dir, bam_file_dir, depth_file_dir, sample_prefix, ref, min_breadth, min_average_depth):
    if not os.path.exists(os.path.join(output_dir, depth_file_dir)):
        os.mkdir(os.path.join(output_dir, depth_file_dir))
        
    depth_f = run_depth(output_dir, bam_file_dir, depth_file_dir, sample_prefix)
    breadth_cov, average_depth_cov = coverage_calc(depth_f, ref)
    print(f"Breadth of Coverage for {sample_prefix}: {breadth_cov}")
    print(f"Depth of Coverage for {sample_prefix}: {average_depth_cov}")
    if breadth_cov >= min_breadth and average_depth_cov >= min_average_depth:
        print(f"{sample_prefix} passed post-alignment QC.")
        return True
    else:
        print(f"{sample_prefix} failed post-alignment QC.")
        return False

def fastqc_stats(output_dir, bam_file_dir, sample_id):
    fastqc_report_dir = os.path.join(output_dir, "fastqc_report")
    if not os.path.exists(fastqc_report_dir):
        os.mkdir(fastqc_report_dir)

    bam_file = os.path.join(output_dir, bam_file_dir, f"{sample_id}.sorted.bam")
    subprocess.run(["fastqc", bam_file, "-f", "bam", "-o", fastqc_report_dir],
                    stdout=open(os.path.join(output_dir, "fastqc.log"), "a"),
                    stderr=open(os.path.join(output_dir, "fastqc.err"), "a"),
                check=True)

def main():
    '''main function'''
    parser = argparse.ArgumentParser(description="Read Alignment using BWA MEM")

    parser.add_argument("sampleId", type=str, help="sample id of the two fastq input file")
    parser.add_argument("outDir", type=str, help="output directory")
    parser.add_argument("bamDir", type=str, help="directory name of bam files in output directory")
    parser.add_argument("ref", type=str, help="reference genome in fasta format")
    parser.add_argument("-min-breadth", type=float, default=0.9,
                        help="minimum breadth of coverage to pass quality control, default: [0.9]")
    parser.add_argument("-min-average-depth", type=int, default=500,
                         help="minimum average depth of coverage to pass quality control, default: [500]")
    args = parser.parse_args()

    sample_prefix = args.sampleId
    output_dir = args.outDir
    bam_file_dir = args.bamDir
    reference_file = args.ref
    min_breadth = args.min_breadth
    min_average_depth = args.min_average_depth

    depth_file_dir = "depth_files"

    coverage_filter(output_dir, bam_file_dir, depth_file_dir, sample_prefix, reference_file, min_breadth, min_average_depth)

if __name__ == "__main__":
    main()