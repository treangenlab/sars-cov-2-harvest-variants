"""This module is responsible for data retriveing, trimming and mapping"""
import os
import subprocess
import argparse

def sort_bam_file(sample_prefix, reference_file, output_dir, bam_files_dir, num_cores, call_indels):
    if (call_indels):
        subprocess.run([
            "lofreq",
            "indelqual",
            "--dindel",
            "-f", f"{reference_file}",
            "-o", os.path.join(output_dir, bam_files_dir, f"{sample_prefix}.indel.bam"),
            os.path.join(output_dir, bam_files_dir, f"{sample_prefix}.bam")],
                    stdout=open(os.path.join(output_dir, "lofreq.log"), "a"),
                    stderr=open(os.path.join(output_dir, "lofreq.err"), "a"),
                    check=True)

        if os.path.exists(os.path.join(output_dir, bam_files_dir, f"{sample_prefix}.bam")):
            os.remove(os.path.join(output_dir, bam_files_dir, f"{sample_prefix}.bam"))
        subprocess.run([
            "mv",
            os.path.join(output_dir, bam_files_dir, f"{sample_prefix}.indel.bam"),
            os.path.join(output_dir, bam_files_dir, f"{sample_prefix}.bam")])

    subprocess.run([
        "samtools",
        "sort",
        "-@", str(num_cores),
        "-o", os.path.join(output_dir, bam_files_dir, f"{sample_prefix}.sorted.bam"),
        "-O", "BAM", os.path.join(output_dir, bam_files_dir, f"{sample_prefix}.bam")],
                    check=True)

    # indexing the sorted bam file for variant caller
    subprocess.run([
        "samtools",
        "index",
        os.path.join(output_dir, bam_files_dir, f"{sample_prefix}.sorted.bam")],
                    check=True)          

def variant_call_lofreq(sample_prefix, reference_file, output_dir, bam_files_dir, vcf_files_dir, num_cores, call_indels):
    '''variant calling using loFreq'''
    if not os.path.exists(os.path.join(output_dir, vcf_files_dir)):
        os.mkdir(os.path.join(output_dir, vcf_files_dir))

    if num_cores == 1:
        lofreq_command = [
                "lofreq",
                "call",
                "-f", f"{reference_file}",
                "--no-default-filter",
                "-o",
                os.path.join(output_dir, vcf_files_dir, f"{sample_prefix}.vcf"),
                os.path.join(output_dir, bam_files_dir, f"{sample_prefix}.sorted.bam")]
    else:
        lofreq_command = [
                "lofreq",
                "call-parallel",
                "--pp-threads", str(num_cores),
                "-f", f"{reference_file}",
                "--no-default-filter",
                "-o",
                os.path.join(output_dir, vcf_files_dir, f"{sample_prefix}.vcf"),
                os.path.join(output_dir, bam_files_dir, f"{sample_prefix}.sorted.bam")]

    if (call_indels):
        lofreq_command.append("--call-indels")

    if os.path.exists(os.path.join(output_dir, vcf_files_dir, f"{sample_prefix}.vcf")):
        os.remove(os.path.join(output_dir, vcf_files_dir, f"{sample_prefix}.vcf"))
    subprocess.run(lofreq_command,
                   stdout=open(os.path.join(output_dir, "lofreq.log"), "a"),
                   stderr=open(os.path.join(output_dir, "lofreq.err"), "a"),
                   check=True)

    return os.path.join(output_dir, vcf_files_dir, f"{sample_prefix}.vcf")

def faidx_reference(reference_file, output_dir):
    if not os.path.exists(f"{reference_file}.fai"):
        subprocess.run(["samtools", "faidx", reference_file],
                    stdout=open(os.path.join(output_dir, "samtools.log"), "a"),
                    stderr=open(os.path.join(output_dir, "samtools.err"), "a"),
                    check=True)

def vcf_quality_control(sample_prefix, vcf_files_dir, output_dir, filtered_vcf_dir, cov_min=20, af_min=0.02):
    if not os.path.exists(os.path.join(output_dir, filtered_vcf_dir)):
        os.mkdir(os.path.join(output_dir, filtered_vcf_dir))

    if os.path.exists(os.path.join(output_dir, filtered_vcf_dir, f"{sample_prefix}.vcf")):
        os.remove(os.path.join(output_dir, filtered_vcf_dir, f"{sample_prefix}.vcf"))
    subprocess.run(["lofreq", "filter",
                    "--cov-min", str(cov_min),
                    "--af-min", str(af_min),
                    "-b", "fdr",
                    "-c", "0.001",
                    "-i", os.path.join(output_dir, vcf_files_dir, f"{sample_prefix}.vcf"),
                    "-o", os.path.join(output_dir, filtered_vcf_dir, f"{sample_prefix}.vcf")],
                   stdout=open(os.path.join(output_dir, "lofreq.log"), "a"),
                   stderr=open(os.path.join(output_dir, "lofreq.err"), "a"),
                   check=True)

def main():
    '''main function'''
    parser = argparse.ArgumentParser(description="Variant Calling with Lofreq")

    parser.add_argument("sampleId", type=str, help="sample id of the input file(s)")
    parser.add_argument("outDir", type=str, help="output directory")
    parser.add_argument("bamDir", type=str, help="directory name of bam files in output directory")
    parser.add_argument("ref", type=str, help="reference genome in fasta format")
    parser.add_argument("-numcore", type=int, default=1,
                        help="Number of processor used for parallelization")
    parser.add_argument('--no-indel', dest='indel', action='store_false',
                        help="Do not call indels")

    parser.set_defaults(indel=True)

    args = parser.parse_args()

    sample_prefix = args.sampleId
    num_cores = args.numcore
    output_dir = args.outDir
    bam_files_dir = args.bamDir
    reference_file = args.ref
    indel_flag = args.indel

    vcf_files_dir= "vcf_files"

    faidx_reference(reference_file, output_dir)
    sort_bam_file(sample_prefix, reference_file, output_dir, bam_files_dir, num_cores, indel_flag)
    variant_call_lofreq(sample_prefix, reference_file, output_dir, bam_files_dir, vcf_files_dir, num_cores, indel_flag)

    filtered_vcf_dir = "vcf_files_filtered"
    vcf_quality_control(sample_prefix, vcf_files_dir, output_dir, filtered_vcf_dir, cov_min=20, af_min=0.02)

if __name__ == "__main__":
    main()
