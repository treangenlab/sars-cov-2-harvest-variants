'''simulate genomes based on read data set'''
import subprocess
import os
import argparse
from collections import defaultdict
import vcf
import numpy as np
from Bio import SeqIO

def mask_reference(output_dir, depth_file_dir, filtered_vcf_dir, sample_prefix, min_depth=20):
    depth_file = os.path.join(output_dir, depth_file_dir, f"{sample_prefix}.depth")
    vcf_file = os.path.join(output_dir, filtered_vcf_dir, f"{sample_prefix}.vcf")
    
    with open(os.path.join(output_dir, f"{sample_prefix}.low.bed"), "w") as out_bed:
        with open(depth_file, "r") as depth_f:
            n_flag = False
            for line in depth_f.readlines():
                chrom, pos, depth = line.strip().split("\t")
                pos = int(pos) - 1
                depth = int(depth)
                if depth < min_depth and not n_flag:
                    out_bed.write(f"{chrom}\t{pos}\t")
                    n_flag = True
                elif depth >= min_depth and n_flag:
                    out_bed.write(f"{pos}\tLOW_DEPTH\n")
                    n_flag = False
            
            if n_flag:
                out_bed.write(f"{pos+1}\tLOW_DEPTH\n")
                
    with open(os.path.join(output_dir, f"{sample_prefix}.del.bed"), "w") as out_bed:
        vcf_reader = vcf.Reader(open(vcf_file, 'r'))
        for record in vcf_reader:
            # deletion
            if len(record.REF) > 1:
                start = int(record.POS)
                end = start + len(record.REF) - 1
                out_bed.write(f"{record.CHROM}\t{start}\t{end}\n")
                
    subprocess.run(["bedtools", "subtract",
                    "-a", os.path.join(output_dir, f"{sample_prefix}.low.bed"),
                    "-b", os.path.join(output_dir, f"{sample_prefix}.del.bed")],
                   stdout=open(os.path.join(output_dir, f"{sample_prefix}.masked.bed"), "w"),
                check=True)

    os.remove(os.path.join(output_dir, f"{sample_prefix}.low.bed"))
    os.remove(os.path.join(output_dir, f"{sample_prefix}.del.bed"))

    return os.path.join(output_dir, f"{sample_prefix}.masked.bed")

def simulate_consensus(sample_prefix, reference_file, vcf_files_dir, output_dir, mask, simulated_genome_dir):
    if not os.path.exists(os.path.join(output_dir, simulated_genome_dir)):
        os.mkdir(os.path.join(output_dir, simulated_genome_dir))
    '''simulate consensus based on vcf file'''
    # vcf file zipping, indexing, and consensus generation
    subprocess.run([
            "bgzip",
            os.path.join(output_dir, vcf_files_dir, f"{sample_prefix}.vcf")],
        check=True)
    subprocess.run([
            "bcftools",
            "index",
            os.path.join(output_dir, vcf_files_dir, f"{sample_prefix}.vcf.gz")],
        check=True)
    subprocess.run([
            "bcftools",
            "consensus",
            os.path.join(output_dir, vcf_files_dir, f"{sample_prefix}.vcf.gz"),
            "-o", os.path.join(output_dir, simulated_genome_dir, f"{sample_prefix}.fasta"),
            "-f", reference_file,
            "-m", mask],
            stdout=open(os.path.join(output_dir, "bcftools.log"), "a"),
            stderr=open(os.path.join(output_dir, "bcftools.err"), "a"),
        check=True)
    # unzip vcf file
    subprocess.run([
            "bgzip",
            "-d",
            os.path.join(output_dir, vcf_files_dir, f"{sample_prefix}.vcf.gz")],
        check=True)
    os.remove(os.path.join(output_dir, vcf_files_dir, f"{sample_prefix}.vcf.gz.csi"))

def vcf_split(sample_prefix, reference_file, output_dir, vcf_files_dir, mask, simulated_genome_dir):
    '''split vcf file into multiple if different alt at same position'''
    def write_vcf_flies():
        for record in vcf_reader:
            if len(record_dict[record.POS]) == 1:
                vcf_writer.write_record(record)
            else:
                try:
                    if record_dict[record.POS][i] == record.ALT:
                        vcf_writer.write_record(record)
                except IndexError:
                    continue

        vcf_writer.close()
        simulate_consensus(f'{sample_prefix}_{i}', reference_file, vcf_files_dir, output_dir, mask, simulated_genome_dir)

    vcf_reader = vcf.Reader(
        filename=os.path.join(output_dir, vcf_files_dir, f"{sample_prefix}.vcf"))
    record_dict = defaultdict(list)

    for record in vcf_reader:
        record_dict[record.POS].append(record.ALT)

    if bool(record_dict):
        max_alt = len(max(record_dict.values(), key=len))
        if max_alt == 1:
            simulate_consensus(sample_prefix, reference_file, vcf_files_dir, output_dir, mask, simulated_genome_dir)
        elif max_alt > 1:
            for i in range(max_alt):
                vcf_f = os.path.join(output_dir, vcf_files_dir, f"{sample_prefix}_{i}.vcf")
                vcf_writer = vcf.Writer(open(vcf_f, 'w'), vcf_reader)
                vcf_reader = vcf.Reader(filename=os.path.join(output_dir, vcf_files_dir, f"{sample_prefix}.vcf"))
                write_vcf_flies()
                if os.path.exists(vcf_f):
                    os.remove(vcf_f)
    else:
        simulate_consensus(sample_prefix, reference_file, vcf_files_dir, output_dir, mask, simulated_genome_dir)