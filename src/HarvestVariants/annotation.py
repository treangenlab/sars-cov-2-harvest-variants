import os
import subprocess
import argparse

def annotation(sample_prefix, vcf_files_dir, output_dir, ann_vcf_files_dir, snpeff_db="NC_045512.2"):
    '''variant annotation using snpEff'''
    if not os.path.exists(os.path.join(output_dir, ann_vcf_files_dir)):
        os.mkdir(os.path.join(output_dir, ann_vcf_files_dir))

    subprocess.run(["snpEff", "ann",
                    snpeff_db,
                    "-noStats",
                    os.path.join(output_dir, vcf_files_dir, f"{sample_prefix}.vcf")],
                    stdout=open(os.path.join(output_dir, ann_vcf_files_dir, f"{sample_prefix}.vcf"), "w"),
                    stderr=open(os.path.join(output_dir, "snpEff.err"), "a"),
                    check=True)

def main():
    '''main function'''
    parser = argparse.ArgumentParser(description="Variant Annotation with snpEff")

    parser.add_argument("sampleId", type=str, help="sample id of the two fastq input file")
    parser.add_argument("outDir", type=str, help="output directory")
    parser.add_argument("--vcfDir", type=str, help="directory name of vcf files in output directory",
                        default="vcf_files_filtered")
    parser.add_argument("--annDir", type=str, help="directory name of annotated vcf files in output directory",
                        default="vcf_files_filtered_ann")
    parser.add_argument("--snpeffdb", type=str, help="snpeff database for the reference genome, default:[NC_045512.2]",
                        default="NC_045512.2")

    args = parser.parse_args()

    sample_prefix = args.sampleId
    output_dir = args.outDir
    vcf_files_dir = args.vcfDir
    ann_vcf_files_dir = args.annDir
    snpeff_db = args.snpeffdb

    annotation(sample_prefix, vcf_files_dir, output_dir, ann_vcf_files_dir, snpeff_db)

if __name__ == "__main__":
    main()