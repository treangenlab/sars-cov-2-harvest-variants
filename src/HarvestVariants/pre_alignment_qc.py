"""This module is responsible for illumina paired-end sequencing data quality control"""
import os
import subprocess
import argparse
import sys

def run_fastp(read_1, read_2, output_prefix, output_dir, log_dir,
              adapter_file,
              num_cores,
              s_window_size, s_window_q_threshold,
              min_len,
              correction,
              base_q_threshold=25, unqualified_limit=40,
              n_base_limit=5,
              low_complexity_threshold=30,
              no_html=True):
    '''trim reads using fastp'''
    if not os.path.exists(os.path.join(output_dir, "trimmed_reads")):
        os.mkdir(os.path.join(output_dir, "trimmed_reads"))
    if not os.path.exists(os.path.join(output_dir, log_dir)):
        os.mkdir(os.path.join(output_dir, log_dir))

    command = ["fastp",
                "--in1", read_1,
                "--out1", os.path.join(output_dir, "trimmed_reads", f"{output_prefix}_1.fastq"),
                "--cut_front", "--cut_tail",
                "--cut_window_size", str(s_window_size),
                "--cut_mean_quality", str(s_window_q_threshold),
                "--qualified_quality_phred", str(base_q_threshold),
                "--unqualified_percent_limit", str(unqualified_limit),
                "--n_base_limit", str(n_base_limit),
                "--length_required", str(min_len),
                "--low_complexity_filter", "--complexity_threshold", str(low_complexity_threshold),
                "--json", os.path.join(output_dir, log_dir, f"{output_prefix}.fastp.log"),
                "--html", os.path.join(output_dir, log_dir, f"{output_prefix}.fastp.html"),
                "--report_title", output_prefix,
                "--thread", str(num_cores)]
    
    if os.path.exists(read_2):
        command = command + ["--in2", read_2, "--out2", os.path.join(output_dir, "trimmed_reads", f"{output_prefix}_2.fastq")]

    if adapter_file != "":
        command.append("--adapter_fasta")
        command.append(adapter_file)

    if correction:
        command.append("--correction")

    subprocess.run(command, check=True)

    if no_html:
        if os.path.exists(os.path.join(output_dir, log_dir, f"{output_prefix}.fastp.html")):
            os.remove(os.path.join(output_dir, log_dir, f"{output_prefix}.fastp.html"))

def main():
    '''main function'''
    parser = argparse.ArgumentParser(description="Quality Control for Illumina PE Sequencing Data")

    parser.add_argument("read1", type=str, help="read 1 input file for paired-end reads")
    parser.add_argument("read2", type=str, help="read 2 input file for paired-end reads")
    parser.add_argument("outPrefix", type=str, help="prefix of the output files")
    parser.add_argument("outDir", type=str, help="output directory")
    parser.add_argument("logDir", type=str, help="log directory")

    parser.add_argument("-adapters", type=str, default="",
                        help="Fasta file containing adapter sequences.")

    # fastp parameters
    parser.add_argument("-slidingWindowSize", type=int, default=4,
                        help="Sliding window size for fastp. Default: [4]")
    parser.add_argument("-slidingWindowQuality", type=int, default=25,
                        help="Sliding window quality threshold for fastp. Default: [25]")
    parser.add_argument("-minLen", type=int, default=15,
                        help="Minimum length required for a read to be valid.  Default: [15]")
    parser.add_argument("-fastpErrorCorrection", dest='correction', action='store_true',
                        help="Enable base correction in overlapped regions using Fastp. Default: [False]")
    # parallelization parameters
    parser.add_argument("-numcore", type=int, default=1,
                        help="Number of processor used for parallelization.  Default: [1]")

    parser.set_defaults(correction=False)
    args = parser.parse_args()

    read_1 = args.read1
    read_2 = args.read2
    output_dir = args.outDir
    output_prefix = args.outPrefix
    log_dir = args.logDir

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    adapter_file = args.adapters

    s_window_size = args.slidingWindowSize
    s_window_q_threshold = args.slidingWindowQuality
    min_len = args.minLen
    correction = args.correction
    num_cores = args.numcore
    
    print("Adapter removal and read trimming with Fastp.")
    if adapter_file == "":
        print("No adapter sequence found, enable adapter auto-detection.")
    else:
        if not os.path.exists(adapter_file):
            print("Adapter file not found.")
            sys.exit(1)
    if correction:
        print("Error correction enabled")
    run_fastp(read_1, read_2, output_prefix, output_dir, log_dir, adapter_file, num_cores, s_window_size, s_window_q_threshold, min_len, correction)

if __name__ == "__main__":
    main()