import os
import subprocess
import argparse

def read_mapping_bwa(sample_prefix, reference_file, num_cores, output_dir):
    '''map the reads to the reference'''
    sam_output_dir = os.path.join(output_dir, "sam_files")
    if not os.path.exists(sam_output_dir):
        os.mkdir(sam_output_dir)

    if os.path.exists(os.path.join(output_dir, "trimmed_reads", f"{sample_prefix}_2.fastq")):
        try:
            subprocess.run([
                "bwa",
                "mem",
                "-t", str(num_cores),
                "-o", os.path.join(sam_output_dir, f"{sample_prefix}.sam"),
                reference_file,
                os.path.join(output_dir, "trimmed_reads", f"{sample_prefix}_1.fastq"),
                os.path.join(output_dir, "trimmed_reads", f"{sample_prefix}_2.fastq")],
                            stdout=open(os.path.join(output_dir, "bwa_mem.log"), "a"),
                            stderr=open(os.path.join(output_dir, "bwa_mem.err"), "a"),
                            check=True)
        except subprocess.CalledProcessError:
            return 1
    else:
        try:
            subprocess.run([
                "bwa",
                "mem",
                "-t", str(num_cores),
                "-o", os.path.join(sam_output_dir, f"{sample_prefix}.sam"),
                reference_file,
                os.path.join(output_dir, "trimmed_reads", f"{sample_prefix}_1.fastq")],
                            stdout=open(os.path.join(output_dir, "bwa_mem.log"), "a"),
                            stderr=open(os.path.join(output_dir, "bwa_mem.err"), "a"),
                            check=True)
        except subprocess.CalledProcessError:
            return 1
    return 0

def covert_samfile(sample_prefix, output_dir, num_cores):
    '''converting and sorting alignment files'''
    bam_files = os.path.join(output_dir, "bam_files")
    sam_files = os.path.join(output_dir, "sam_files")
    if not os.path.exists(bam_files):
        os.mkdir(bam_files)

    # covert sam file to binary bam file
    subprocess.run([
        "samtools",
        "view",
        "-@", str(num_cores),
        "-bS", os.path.join(sam_files, f"{sample_prefix}.sam"),
        "-o", os.path.join(bam_files, f"{sample_prefix}.bam")],
                    check=True)

def index_reference(ref, output_dir):
    if not (os.path.exists(f"{ref}.bwt") and os.path.exists(f"{ref}.ann") and \
            os.path.exists(f"{ref}.amb") and os.path.exists(f"{ref}.pac") and \
            os.path.exists(f"{ref}.sa")):
        subprocess.run(["bwa", "index", ref], 
                        stdout=open(os.path.join(output_dir, "bwa_mem.log"), "a"),
                        stderr=open(os.path.join(output_dir, "bwa_mem.err"), "a"),
                        check=True)

def main():
    '''main function'''
    parser = argparse.ArgumentParser(description="Read Alignment using BWA MEM")

    parser.add_argument("sampleId", type=str, help="sample id of the two fastq input file")
    parser.add_argument("ref", type=str, help="reference genome in fasta format")
    parser.add_argument("outDir", type=str, help="output directory")

    # parallelization parameters
    parser.add_argument("-numcore", type=int, default=1,
                        help="Number of processor used for parallelization.  Default: [1]")

    parser.set_defaults(dedup=False)
    args = parser.parse_args()

    sample_prefix = args.sampleId
    reference_file = args.ref
    output_dir = args.outDir

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    num_cores = args.numcore
    
    index_reference(reference_file, output_dir)
    read_mapping_bwa(sample_prefix, reference_file, num_cores, output_dir)
    covert_samfile(sample_prefix, output_dir, num_cores)

if __name__ == "__main__":
    main()