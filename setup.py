import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="HarvestVariants-bkille",
    version="0.0.1",
    author="Bryce Kille",
    author_email="brycekille@gmail.com",
    description="A pipeline that handles data downloading, quality control, read mapping, intra-host variant calling, genome phasing and MSA for SARS-CoV-2 SRAs and nucleotide records.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/treangenlab/sars-cov-2-harvest-variants",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=['HarvestVariants'],
    entry_points={
        "console_scripts": [
            'hv-pipeline.py = HarvestVariants.pipeline:main',
            'sra2vcf.py = HarvestVariants.sra2vcf:main']},
    python_requires=">=3.7",
)
