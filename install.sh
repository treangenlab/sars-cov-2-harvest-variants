#!/bin/bash
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge

conda install -c conda-forge biopython">=1.79"
conda install -c bioconda pyvcf">=0.6.8" sra-tools"=2.10.9" parsnp"=1.7.4" fastp">=0.22" samtools">=1.13" bcftools">=1.8" bwa">=0.7.17" lofreq">=2.1.3" bedtools">=2.30.0" snpeff">=5.0" fastqc">=v0.11.9"

python setup.py install
